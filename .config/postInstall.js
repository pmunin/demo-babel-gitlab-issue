//TODO: extract jsCommander to separate node_module
return;

//from here: https://github.com/npm/npm/issues/10926

let fs = require('fs');
let path=require('path')

//Windows: mklink /D /j node_modules\\jsCommander src\\components\\jsCommander

console.log("Resolve '.' =", path.resolve("."));

let destination = path.resolve("./node_modules/jsCommander");
console.log("Destination:", destination)
let source = path.resolve('./src/components/jsCommander');
console.log("Source:", source)
if(fs.existsSync(destination))
{
    console.log("Removing exising link: ",destination)
    fs.unlinkSync(destination)
}
else
{
    console.log("Link doesn't exist, nothing to remove. ", destination)
}
console.log("Creating junction link: "+source+" => "+destination);
fs.symlinkSync(source, destination, 'junction');