import {isPrimitive} from 'util'
export function stringify(value:any)
{
    var traversedObjects = new Map();
    let lastId = 0;
    return JSON.stringify(value, (k: string, v: any) => {
        if(isPrimitive(v)) return v;
        if(Array.isArray(v)) return v;
        if(v instanceof Object && v.constructor === Object) return v;

        if(traversedObjects.has(v))
            return {"$ref":traversedObjects.get(v)};
        traversedObjects.set(v,lastId++);
        
        let res = Object.assign({ '$type':v.constructor.name},v) ;
        if(typeof v[Symbol.iterator] === "function")
        {
            res["$iterator"] = Array.from(v[Symbol.iterator]());
        }
        if(v instanceof Function) {
            const f:Function = v;
            res["name"] = f.name;
        }
        let toString = v.toString() as String;
        if(!toString.startsWith("[object "))
            res["$toString"] = v.toString();
        
        return res;
    }, 2);
    //console.log(configLog);

}