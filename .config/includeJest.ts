import * as webpack from 'webpack'

const jestWebpackMain = require('jest-webpack/src/jest-webpack')
export function includeJest(config:webpack.Configuration, env:any)
{
    jestWebpackMain(process.argv, config)
}
