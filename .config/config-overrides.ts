//see here: https://github.com/timarney/react-app-rewired/blob/master/packages/react-app-rewired/config-overrides.js
import { isPrimitive } from 'util';
import { stringify } from './stringify';

import * as webpack from 'webpack';
import * as path from 'path';
import * as _ from 'lodash';
import * as fs from 'fs';
import {includeJest} from './includeJest';


import * as paths from 'react-scripts/config/paths';
function merger(src, mergee) {
    if (_.isArray(src) || _.isArray(mergee)) {
        return [].concat(src, mergee);
    }
}

function findTsLoader(rules:any[])
{
    return rules.find((r: any) => r.loader && r.loader.indexOf("ts-loader") >= 0) as webpack.NewLoaderRule;
}
function webPackOverride(config: webpack.Configuration, env: any) 
{
    console.log("webpackOverride executing for environment:", env)
    const module = config.module as webpack.NewModule;

    let tsloaderRuleItem = findDeep({children: module.rules}, (i:webpack.NewLoaderRule)=>(i.loader&&i.loader.toString().indexOf("ts-loader")>=1),i=>i.oneOf);
    let tsLoaderRule = tsloaderRuleItem.item as webpack.NewLoaderRule;

    tsLoaderRule.loader = [
        "babel-loader",
        tsLoaderRule.loader
    ] as any;

    let tsLoaderIndex= tsloaderRuleItem.parentArgs.children.indexOf(tsLoaderRule);
    tsloaderRuleItem.parentArgs.children.splice(tsLoaderIndex,0,{
       test:/\.(js|jsx)$/,
       loader:"babel-loader",
    //    options:{
    //     babelrc: true,
    //     // presets: [
    //     //     require.resolve("babel-preset-react-app")
    //     //   //"c:\\temp\\demo-babel-gitlab-issue\\node_modules\\babel-preset-react-app\\index.js"
    //     // ],
    //     compact: true
    //   }
    } as webpack.NewLoaderRule);
    

    writeLog(config,env)
    return config;
}


function writeLog(config, env:any="")
{
    let strLog = stringify(config);
    if(!fs.existsSync("logs"))
        fs.mkdirSync("logs");
    fs.writeFileSync(`logs/webpack-${env}.log`, strLog);
    console.log(env+" config:",strLog);
}


type FindDeepArgs = {
    parentArgs?:FindDeepArgs;
    children?:any[];
    item?:any;
}
function findDeep(args:FindDeepArgs,predicate:(item:any, parentArgs?:FindDeepArgs)=>boolean, getChildren:(item:any, parentArgs?:FindDeepArgs)=>any[], ignore:Set<any>=undefined)
:FindDeepArgs
{
    if(!ignore) ignore = new Set();
    for(let item of args.children)
    {
        if(!isPrimitive(item))
        { 
            if(ignore.has(item)) continue;
            else ignore.add(item);
        }

        let itemArgs:FindDeepArgs = { item, parentArgs:args};
        if(predicate(item,args)) return itemArgs;

        itemArgs.children = getChildren(item,args);
        if(Array.isArray(itemArgs.children))
        {
            let childRes = findDeep( itemArgs, predicate, getChildren, ignore);
            if(childRes) return childRes;
        }
    }
}


function jestOverride(config, env:any="test")
{
    console.log("jest override executing")
    //delete config.testMatch;
    //config.testPathPattern = "include";
    // config.transform["^.+\\.jsx?$"] = "<rootDir>/node_modules/babel-jest";
    // config.transform["^.+\\.tsx?$"] = "<rootDir>/node_modules/ts-jest/preprocessor.js";
    config.transform["^.+\\.jsx?$"] = path.resolve("node_modules/babel-jest");
    config.transform["^.+\\.tsx?$"] = path.resolve("node_modules/ts-jest/preprocessor.js");
    // "^.+\\.tsx?$"
    // "^.+\\.tsx?$"

    writeLog(config,env);
    return config;
}



module.exports = 
//webPackOverride;
    {
        webpack: webPackOverride,
        jest:jestOverride,
        devServer: (baseCreateConfig:Function)=>{
            return function(){
                const baseRes = baseCreateConfig.apply(this,arguments);
                //baseRes.quiet = false;
                //baseRes.hot=false;
                writeLog(baseRes,"devServer");
                return baseRes;
            }
        }
    }

